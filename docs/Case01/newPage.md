# H123
Test Feed Back

| column0    | column1  | column2 | column3 | column4 | column5 |
| ---------- | -------- | ------- | ------- | ------- | ------- |
| 月         | 日期     | AR      | HB      | ON      | 總計    |
| 2022/10/18 |          | 6       |         | 11      | 17      |
| 10月       | 10月18日 | 15      | 40      | 16      | 71      |
|            | 10月19日 |         | 15      | 8       | 23      |
|            | 10月20日 |         | 51      | 13      | 64      |
|            | 10月21日 |         | 29      | 9       | 38      |
|            | 10月22日 |         | 65      | 15      | 80      |
|            | 10月23日 |         | 45      | 13      | 58      |
|            | 10月24日 |         | 26      | 10      | 36      |
|            | 10月25日 |         | 18      | 10      | 28      |
|            | 10月26日 |         | 23      | 11      | 34      |
|            | 10月27日 |         | 28      | 10      | 38      |
|            | 10月28日 |         | 17      | 6       | 23      |
|            | 10月29日 |         | 50      | 20      | 70      |
|            | 10月30日 |         | 40      | 20      | 60      |
|            | 10月31日 |         | 42      | 10      | 52      |
| 10月 合計  |          | 15      | 489     | 171     | 675     |
| 11月       | 11月1日  |         | 28      | 9       | 37      |
|            | 11月2日  |         | 61      | 12      | 73      |
|            | 11月3日  |         | 19      | 10      | 29      |
|            | 11月4日  |         | 23      | 13      | 36      |
|            | 11月5日  |         | 114     | 17      | 131     |
|            | 11月6日  |         | 58      | 16      | 74      |
| 11月 合計  |          |         | 303     | 77      | 380     |
| 總計       |          | 21      | 792     | 259     | 1072    |


<iframe width="560" height="315" src="https://www.youtube.com/embed/EOAPMhaCtuw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

![Alt text](../assets/1.png)

[label](../TEST/aigenerate.md)


| 51  | 13  |
| --- | --- |
| 29  | 9   |
| 65  | 15  |
| 45  | 13  |
| 26  | 10  |
| 18  | 10  |
| 23  | 11  |
| 28  | 10  |
| 17  | 6   |
| 50  | 20  |
| 40  | 20  |
| 42  | 10  |

!!! note "Phasellus posuere in sem ut cursus"

    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

???+ note "Phasellus posuere in sem ut cursus"

    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

- ==This was marked==
- ^^This was inserted^^
- ~~This was deleted~~

++ctrl+alt+del++

!!! example

- H~2~O
- A^T^A

```mermaid
graph TD;
    A-->B;
    A-->C;
    B-->D;
    C-->D;
```

- [ ] 123
- [ ] 123
- [ ] 123

- 12
  - 123
  - 123
- 11df
- 55688
- [ ] why
- [x] HOW
- [ ] CHECK

- [H123](#h123)


| column0 | column1 |
| ------- | ------- |
| 18      | 10      |
| 23      | 11      |
| 28      | 10      |
| 17      | 6       |
| 50      | 20      |
